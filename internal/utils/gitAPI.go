/*
Copyright © 2021  BJOERN MATHIS

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package utils

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

// checks if the current directory is a git repo by running git status
func isGitRepo() bool {
	_, err := exec.Command("git", "status").Output()

	return err == nil
}

// tries to get the current git tag, if it fails 0.0.1 is returned
func getTag() string {
	out, err := exec.Command("git", "describe", "--abbrev=0", "--tags").Output()

	if err != nil {
		return "0.0.1"
	}
	return strings.TrimSpace(string(out))
}

// tries to get the current git branch, exists the program on failure
func getBranch() string {
	out, err := exec.Command("git", "rev-parse", "--abbrev-ref", "HEAD").Output()

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error while trying to get branch: %s\n", err)
		os.Exit(1)
	}
	return strings.TrimSpace(string(out))
}

// tries to get the current git has in short form, exists the program on failure
func getHash() string {
	out, err := exec.Command("git", "rev-parse", "--short", "HEAD").Output()

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error while trying to get hash: %s\n", err)
		os.Exit(1)
	}
	return strings.TrimSpace(string(out))
}
